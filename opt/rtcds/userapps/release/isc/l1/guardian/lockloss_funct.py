import os
#from gwpy import timeseries
import cdsutils
import numpy as np

def log_lockloss_changes(channels_data, duration, start, log_file):
    """
    Log the changes that happen during lockloss as well at the times at which
    they happen.
    The following channels are analyzed:
        index 0: "L1:PEM-CS_ADC_4_11_OUT_DQ"
        index 1: "L1:LSC-ASVAC_B_LF_OUT_DQ"
        index 2: "L1:LSC-POP_A_LF_OUT_DQ"

    Args:
        channels_data (dict):
            Dictionary with the data from the three channels in arrays.
        lockloss_time (float):
            At which time in GPS seconds did the lockloss happen.
        duration (float):
            Amount time in seconds that are loaded in the channels data.
            Used to compute medians.
    """

    # Arrays of times used to compute at which moment each peak occurs.
    times_2e14 = np.arange(start, start+duration, 1/4096)
    times_2e16 = np.arange(start, start+duration, 1/16384)

    # L1:PEM-CS_ADC_4_11_OUT_DQ
    try: # added try accept to avoid bug when data isn't there (TJO 1/12/2025   
        adc_data = channels_data[0].data
    except:
        return True
    peak_power_index = np.argmax(adc_data)
    peak_power_time = times_2e14[peak_power_index]
    HAM6_before_power = np.median(adc_data[0:peak_power_index]) 

    # Integrating the power change during lockloss.
    # Starting when the power is double that of the plateau. and finishing
    # when it is half the plateau.
    try: #Edge case: computer failures made the folowing array searches fail, wrap in a try/except to avoid issue
        peak_start_index = np.where(adc_data > (HAM6_before_power * 2))[0][0] 
        peak_end_index = np.where((adc_data < (HAM6_before_power * 0.5)) & (times_2e14 > peak_power_time))[0][0]
    except:
        return True
    times_peak = times_2e14[peak_start_index:peak_end_index]
    adc_peak_data = adc_data[peak_start_index:peak_end_index]
    integrated_peak_power = np.trapz(adc_peak_data, x=times_peak)

    peak_power = np.max(adc_data)
    ten_percent_power = 0.1 * peak_power
    ten_percent_index = np.where(adc_data >= ten_percent_power)[0][0]
    ten_percent_time = times_2e14[ten_percent_index]

    # L1:LSC-ASVAC_B_LF_OUT_DQ
    asvac_data = channels_data[1].data
    peak_light_index = np.argmax(asvac_data)
    shutter_time = times_2e16[peak_light_index]

    # L1:LSC-POP_A_LF_OUT_DQ
    pop_data = channels_data[2].data
    pop_power = np.mean(pop_data[0:peak_light_index])

    data_to_append = np.array(
        [
            int(peak_power_time),
            pop_power,
            integrated_peak_power,
            shutter_time,
            ten_percent_time,
            peak_power_time,
            HAM6_before_power
        ]
    )

    # Write the file with a header in case it does not exist
    if not os.path.exists(log_file):
        with open(log_file, 'w') as output_file:
            output_file.write("# GPS time (s), POP power (W), Integrated HAM6 LockLoss power (J), Time of shutter (GPS s), 10% peak HAM6 LockLoss power time (GPS s), Peak HAM6 LockLoss power time (s), HAM6 power before LockLoss (W)")

    # Append data to log file
    with open(log_file, "a+") as output_file:
        output_file.write("\n")
        np.savetxt(output_file, data_to_append, newline=", ", fmt="%s")


if __name__ == "__main__":
    # test cases
    for lockloss_time in [1412580073,1412565265,1412431822,1412383255,1412275031,1412156747,1412087029,1412079432,1412069641,1411940051,1411824119,1411736529,1411593389,1411529274,1411501703,1411441959,1411432127,1411418851,1411415717,1411408312,1411404069,1411365126,1411292037,1411289224,1411282722,1411223114,1411178077,1411103405,1411092789,1411024342,1410967374,1410960630]:

        channels = ["L1:PEM-CS_ADC_4_11_OUT_DQ", "L1:LSC-ASVAC_B_LF_OUT_DQ", "L1:LSC-POP_A_LF_OUT_DQ"]
        channels_data = cdsutils.getdata(channels,100,start=lockloss_time-95)
        log_lockloss_changes(
            channels_data=channels_data,
            duration=100,
            start=lockloss_time-95,
            log_file="/opt/rtcds/userapps/release/isc/l1/scripts/lockloss_power_history_test.csv"
        )

