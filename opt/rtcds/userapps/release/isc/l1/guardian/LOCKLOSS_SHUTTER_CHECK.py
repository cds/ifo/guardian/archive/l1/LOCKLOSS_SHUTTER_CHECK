#! /usr/bin/env python

from guardian import GuardState, GuardStateDecorator
import cdsutils as cdu
from gpstime import gpstime
import time
from lockloss_funct import log_lockloss_changes

request = 'HIGH_ARM_POWER'
nominal = 'HIGH_ARM_POWER'

# we need to set these thresholds so that we only go to check shutter if and only if the shutter should have been triggered
arm_power_lower_thresh = 22e3
arm_power_upper_thresh = 25e3
minimum_arm_power_thresh = 500
#calibration of these PDs by Marie K. is in LLO alog 25777
#3.7 ppm ETM transmission, 0.025 transmission along beam path to PDs, 3.6e-7 watts/count or 2,777,777 counts/watt at PD, analog gain of 3dB (+ 1 stage of whitening digitally compensated)
watts_circ_per_count=1/(2777777*0.025*2*3.6e-6)  #circulating power per count at output of ASC_X_TR_SUM
x_tr_chan='ASC-X_TR_B_NSUM_OUTPUT'
y_tr_chan='ASC-Y_TR_B_NSUM_OUTPUT'
input_power_readback = 'IMC-PWR_IN_OUTPUT'

class LOW_ARM_POWER(GuardState):
    index = 1
    def run(self):
        circ_power_x=ezca[x_tr_chan]*watts_circ_per_count
        circ_power_y=ezca[y_tr_chan]*watts_circ_per_count
        if circ_power_x >=arm_power_upper_thresh and circ_power_y >=arm_power_upper_thresh:
            return 'HIGH_ARM_POWER'

class HIGH_ARM_POWER(GuardState):
    index = 10
    def run(self):
        circ_power_x=ezca[x_tr_chan]*watts_circ_per_count
        circ_power_y=ezca[y_tr_chan]*watts_circ_per_count
        circ_power = min(circ_power_x,circ_power_y)
        requested_power=ezca[input_power_readback]
        if requested_power < 13.0 and circ_power < arm_power_lower_thresh and circ_power > minimum_arm_power_thresh:
            return 'LOW_ARM_POWER'
        elif circ_power < arm_power_lower_thresh:
            return 'CHECK_SHUTTER'
        else:
            return True

class CHECK_SHUTTER(GuardState):
    index = 20
    def main(self):
        self.timenow = gpstime.tconvert('now').gps()
        self.timer['datawait'] = 5

    def run(self):

        if self.timer['datawait']:
            try:
                gs13data = cdu.getdata(['L1:ISI-HAM6_GS13INF_V1_IN1_DQ','L1:SYS-MOTION_C_SHUTTER_G_TRIGGER_VOLTS'],12,self.timenow-10)
                lockloss_power = cdu.getdata(["L1:PEM-CS_ADC_4_11_OUT_DQ", "L1:LSC-ASVAC_B_LF_OUT_DQ", "L1:LSC-POP_A_LF_OUT_DQ"],100,start=self.timenow-95)
            except:
                log('Failed to get data')
                return 'SHUTTER_FAIL'
            log_lockloss_changes(
                channels_data=lockloss_power,
                duration=100,
                start=self.timenow-95,
                log_file="/opt/rtcds/userapps/release/isc/l1/scripts/lockloss_power_history.csv"
            )
            if max(abs(gs13data[0].data)) > 3e4:
                log('GS13 saw a kick.  Peak GS13 signal = ' + str(max(abs(gs13data[0].data))))
                return 'LOW_ARM_POWER'
            elif max(abs(gs13data[1].data)) < 1.5:
                log('Trigger PD did not reach threshold, peak voltage = ' + str(max(abs(gs13data[1].data))))
                return 'SHUTTER_NO_TRIGGER'
            else:
                log('No kick on HAM6, but should have. Peak GS13 signal = ' + str(max(abs(gs13data[0].data))))
                return 'SHUTTER_FAIL'


class SHUTTER_FAIL(GuardState):
    index=30
    def main(self):
        notify('Check shutter for possible physical failure! Do not proceed!')

class SHUTTER_NO_TRIGGER(GuardState):
    index=40
    def main(self):
        notify('Check trigger photodiodes signals.  See OPS wiki')

edges = [
    ('INIT','LOW_ARM_POWER'),
    ('LOW_ARM_POWER','HIGH_ARM_POWER'),
    ('HIGH_ARM_POWER', 'CHECK_SHUTTER'),
    ('CHECK_SHUTTER', 'SHUTTER_FAIL'),
    ('CHECK_SHUTTER', 'SHUTTER_NO_TRIGGER')
]
